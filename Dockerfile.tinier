FROM golang:1.16 AS builder

RUN apt-get update; \
    apt-get install xz-utils

RUN mkdir -p /app
COPY main.go /app/
COPY go.* /app/

WORKDIR /app

RUN CGO_ENABLED=0 GOOS=linux go build -ldflags="-s -w" -o dist/service main.go

ADD https://github.com/krallin/tini/releases/download/v0.19.0/tini-static /tini
RUN chmod +x /tini

## -----------------

FROM scratch

EXPOSE 8000
COPY --from=builder /app/dist/service /service
COPY --from=builder /tini /tini

ENTRYPOINT ["/tini", "--"]
CMD ["/service"]